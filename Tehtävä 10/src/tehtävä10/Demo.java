package tehtävä10;


public class Demo {
	   private static AbstractBoss getChainOfLoggers(){

		   AbstractBoss lähiEsimies = new LähiEsimies(AbstractBoss.L);
		   AbstractBoss yksikönPäle = new YksikönPäle(AbstractBoss.YP);
		   AbstractBoss yrityksenTJ = new YrityksenTJ(AbstractBoss.YTJ);

		      lähiEsimies.setNextBoss(yksikönPäle);
		      yksikönPäle.setNextBoss(yrityksenTJ);

		      return lähiEsimies;	
		   }

		   public static void main(String[] args) {
		      AbstractBoss loggerChain = getChainOfLoggers();

		      loggerChain.PKP(AbstractBoss.L, 
		         "Lähiesimies pyyntö.");

		      loggerChain.PKP(AbstractBoss.YP, 
		         "Yrityksenpäälikkö pyyntö");

		      loggerChain.PKP(AbstractBoss.YTJ, 
		         "Yrityksentoimitusjohtaja pyyntö");
		   }
}
