package tehtävä10;

public abstract class AbstractBoss {
	
	public static int L = 2; //Lähiesimies
	   public static int YP = 4; //YksikönPäälikkö
	   public static int YTJ = 5; //YrityksenToimitusJohtaja
	   
	   protected int taso;
	   
	   protected AbstractBoss nextBoss;
	   
	   public void setNextBoss(AbstractBoss nextBoss) {
		   this.nextBoss = nextBoss;
	   }
	   
	   public void PKP(int taso, String pyyntö) {  //PalkanKorotusPyyntö
		   if(this.taso <= taso) {
			   write(pyyntö);
		   }
		   if(nextBoss !=null) {
			   nextBoss.PKP(taso, pyyntö);
		   }
	   } 
	   
	   abstract protected void write(String message);
}
