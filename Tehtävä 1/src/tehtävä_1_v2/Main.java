package teht�v�_1_v2;

public class Main {
	
	public static void main(String[] args) {
		OppilasTehdas ot = new OppilasTehdas();
		
		Ruoka ruoka1 = ot.getRuoka("MAKAROONILAATIKKO");
		Juoma juoma1 = ot.getJuoma("Maito");
		ruoka1.syo();
		juoma1.juo();
		
		Ruoka ruoka2 = ot.getRuoka("GRILLI");
		Juoma juoma2 = ot.getJuoma("Kotikalja");
		ruoka2.syo();
		juoma2.juo();
		
		Ruoka ruoka3 = ot.getRuoka("PEKONI");
		Juoma juoma3 = ot.getJuoma("Vesi");
		ruoka3.syo();
		juoma3.juo();
	}

}
