package teht�v�_1_v2;

public class OppilasTehdas {
	public Ruoka getRuoka(String ruokaType) {
		if(ruokaType == null) {
			return null;
		}
		if(ruokaType.equalsIgnoreCase("GRILLI")) {
			return new Grilli();
		}
		else if(ruokaType.equalsIgnoreCase("MAKAROONILAATIKKO")) {
			return new Makaroonilaatikko();
		}
		else if(ruokaType.equalsIgnoreCase("PEKONI")) {
			return new Pekoni();
		}
		return null;
	}
	public Juoma getJuoma(String juomaType) {
		if(juomaType == null) {
			return null;
		}
		if(juomaType.equalsIgnoreCase("MAITO")) {
			return new Maito();
		}
		else if(juomaType.equalsIgnoreCase("KOTIKALJA")) {
			return new Kotikalja();
		}
		else if(juomaType.equalsIgnoreCase("VESI")) {
			return new Vesi();
		}
		return null;
	}


}
