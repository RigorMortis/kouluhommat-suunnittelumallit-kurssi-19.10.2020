package teht�v�2;

public class BossVaateTehdas extends AbstractFactory {
	@Override
	public Vaate getShape(String shapeType) {
		if(shapeType.equalsIgnoreCase("BOSS_HOUSUT")) {
			return new Boss_housut();
		}else if (shapeType.equalsIgnoreCase("BOSS_LIPPIS")) {
			return new Boss_lippis();
		}else if (shapeType.equalsIgnoreCase("BOSS_TPAITA")) {
			return new Boss_tpaita();
		}else if (shapeType.equalsIgnoreCase("BOSS_KENGAT")) {
			return new Boss_kengat();
		}
		return null;
	}

}
