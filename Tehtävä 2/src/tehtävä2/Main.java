package teht�v�2;

public class Main {
	public static void main(String[] args) {
		
		System.out.print("Min� olen Jasper-java-koodaaja, olen juuri aloittanut insinoori opintoni ja minulla on paalle mageet kuteet!\n");
		
	      //haetaan tavallinen vaate tehdas
	      AbstractFactory vaateFactory = FactoryProducer.getFactory(false);
	      
	      //haetaan lippis
	      Vaate lippis = vaateFactory.getShape("LIPPIS");
	      //puetaan lippis paalle
	      lippis.pue();
	      
	      //haetaan farmarit
	      Vaate farmarit = vaateFactory.getShape("FARMARIT");
	       //puetaan farmarit paalle
	      farmarit.pue();
	      
	      //haetaan farmarit
	      Vaate kengat = vaateFactory.getShape("KENGAT");
	       //puetaan kengat paalle
	      kengat.pue();
	      
	      //haetaan t-paita
	      Vaate t_paita = vaateFactory.getShape("T_PAITA");
	      //puetaan t-paita paalle
	      t_paita.pue();
	      
	      
	      System.out.print("\n\nInsin��ri opinnot on nyt takana, nama vasta ovatkin mageet kuteet! En aio kayttaa mitaan muuta kuin bossin vaatteita!!\n");
	      
	      //haetaan boss vaate tehdas
	      AbstractFactory vaateFactory1 = FactoryProducer.getFactory(true);
	      
	      //haetaan boss housut
	      Vaate boss_housut = vaateFactory1.getShape("BOSS_HOUSUT");
	      //puetaan boss housut p��lle
	      boss_housut.pue();
	      
	      //haetaan boss lippis
	      Vaate boss_lippis = vaateFactory1.getShape("BOSS_LIPPIS");
	      //puetaan boss lippis p��lle
	      boss_lippis.pue();
	      
	    //haetaan boss kengat
	      Vaate boss_kengat = vaateFactory1.getShape("BOSS_KENGAT");
	      //puetaan boss kengat p��lle
	      boss_kengat.pue();
	      
	    //haetaan boss tpaita
	      Vaate boss_tpaita = vaateFactory1.getShape("BOSS_TPAITA");
	      //puetaan boss lippis p��lle
	      boss_tpaita.pue();
	      
	      
	      
	   }
}
