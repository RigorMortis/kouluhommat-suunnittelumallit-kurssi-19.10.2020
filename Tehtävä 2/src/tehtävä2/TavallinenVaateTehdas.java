package teht�v�2;

public class TavallinenVaateTehdas extends AbstractFactory {
	@Override
	public Vaate getShape(String shapeType) {
		if(shapeType.equalsIgnoreCase("FARMARIT")) {
			return new Farmarit();
		}else if (shapeType.equalsIgnoreCase("LIPPIS")) {
			return new Lippis();
		}else if (shapeType.equalsIgnoreCase("T_paita")) {
			return new T_paita();
		}else if (shapeType.equalsIgnoreCase("Kengat")) {
			return new Kengat();
		}
		return null;
	}

}
