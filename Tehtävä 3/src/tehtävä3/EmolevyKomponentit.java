package tehtävä3;

import java.util.ArrayList;
import java.util.List;

// tää on siis koosteolio, (composite) Picture opettajan esimerkissä
//- määrittelee koosteolion käyttäytymisen
//- pitää tallessa lapsisolmut
//- toteuttaa lapsiin liittyvät rajapintaoperaatiot



public class EmolevyKomponentit implements Tietokone {
	private Integer id;
	private String name;
	
	private List<Tietokone> emolevynOsat;
	
	public EmolevyKomponentit(Integer id, String name) {
		this.id = id;
		this.name = name;
		this.emolevynOsat = new ArrayList<>();
	}
	
	public void printTietokoneOsanHinta() {
		emolevynOsat.forEach(Tietokone::printTietokoneOsanHinta);
	}
	
	public void lisääEmolevynOsa(Tietokone tietokone) {
		emolevynOsat.add(tietokone);
	}
	public void poistaEmolevynOsa(Tietokone tietokone) {
		emolevynOsat.remove(tietokone);
	}

	//emolevy
    Tietokone emolevyExtraTuulettimet = new EmolevyExtraTuulettimet(
    		1, "EmolevyExtraTuulettimet");
    Tietokone emolevyValaistus = new EmolevyValaistus(
    		2, "EmolevyValaistus");
    Tietokone emolevyVesiJäähdytys = new EmolevyVesiJäähdytys(
    		3, "EmolevyVesiJäähdytys");
    Tietokone emolevy = new Emolevy(
    		4, "Emolevy");
    
	@Override
	public Integer printTietokoneOsanHintaRaaka() {
		int emolevyhinta = emolevy.printTietokoneOsanHintaRaaka();
		int emolevyVesiJäähdytyshinta = emolevyVesiJäähdytys.printTietokoneOsanHintaRaaka();
		int emolevyValaistushinta = emolevyExtraTuulettimet.printTietokoneOsanHintaRaaka();
		int emolevyExtraTuulettimethinta = emolevyValaistus.printTietokoneOsanHintaRaaka();
		int summa = emolevyhinta + emolevyVesiJäähdytyshinta + emolevyValaistushinta + emolevyExtraTuulettimethinta;
		// TODO Auto-generated method stub
		return summa;
	}

}
