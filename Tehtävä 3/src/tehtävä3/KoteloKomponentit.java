package teht�v�3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// t�� on siis koosteolio, (composite) Picture opettajan esimerkiss�
//- m��rittelee koosteolion k�ytt�ytymisen
//- pit�� tallessa lapsisolmut
//- toteuttaa lapsiin liittyv�t rajapintaoperaatiot

public class KoteloKomponentit implements Tietokone {
	private Integer id;
	private String name;
	
	private List<Tietokone> kotelonOsat;
	
	public KoteloKomponentit(Integer id, String name) {
		this.id = id;
		this.name = name;
		this.kotelonOsat = new ArrayList<>();
	}
	
	public void printTietokoneOsanHinta() {
		kotelonOsat.forEach(Tietokone::printTietokoneOsanHinta);
	}
	
	public void lis��KotelonOsa(Tietokone tietokone) {
		kotelonOsat.add(tietokone);
	}
	public void poistaKotelonOsa(Tietokone tietokone) {
		kotelonOsat.remove(tietokone);
	}

	//kotelo
    Tietokone koteloTarrat = new KoteloTarrat(
      1, "KoteloTarrat");
    Tietokone koteloTuulettimet = new KoteloTuulettimet(
      2, "KoteloTuulettimet");
    Tietokone koteloValaistus = new KoteloValaistus(
      3, "KoteloValaistus");
    Tietokone kotelo = new Kotelo(
      4, "Kotelo");
    
	@Override
	public Integer printTietokoneOsanHintaRaaka() {
		int kotelohinta = kotelo.printTietokoneOsanHintaRaaka();
		int koteloTarrathinta = koteloTarrat.printTietokoneOsanHintaRaaka();
		int koteloTuulettimethinta = koteloTuulettimet.printTietokoneOsanHintaRaaka();
		int koteloValaistushinta = koteloValaistus.printTietokoneOsanHintaRaaka();
		int summa = kotelohinta + koteloTarrathinta + koteloTuulettimethinta + koteloValaistushinta;
		// TODO Auto-generated method stub
		return summa;
	}

}


/*
/** "Composite" */
/*
class Kotelo implements Tietokone {
    //Collection of child graphics.
    private final ArrayList<Tietokone> childKotelo = new ArrayList<>();

    //Adds the graphic to the composition.
    public void add(Tietokone kotelo) {
        childKotelo.add(kotelo);
    }
    
    //Prints the graphic.
    @Override
    public void printTietokoneOsanHinta() {
        for (Tietokone kotelo : childKotelo) {
            kotelo.print();  //Delegation
        }
    }

	
}
*/