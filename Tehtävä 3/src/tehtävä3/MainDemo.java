package tehtävä3;



public class MainDemo {
	public static void main(String args[]) {
		
		//perus komponentit
		Tietokone muistipiiri = new Muistipiiri(
		  1, "Muistipiiri");
		Tietokone näytönojain = new Näytönohjain(
		  1, "Näytönohjain");
		Tietokone prosessori = new Prosessori(
		  1, "Prosessori");
		Tietokone verkkokortti = new Verkkokortti(
		  1, "Verkkokortti");
		
		
		//kotelo
        Tietokone koteloTarrat = new KoteloTarrat(
          1, "KoteloTarrat");
        Tietokone koteloTuulettimet = new KoteloTuulettimet(
          2, "KoteloTuulettimet");
        Tietokone koteloValaistus = new KoteloValaistus(
          3, "KoteloValaistus");
        Tietokone kotelo = new Kotelo(
          4, "Kotelo");
        
        
        //emolevy
        Tietokone emolevyExtraTuulettimet = new EmolevyExtraTuulettimet(
        		1, "EmolevyExtraTuulettimet");
        Tietokone emolevyValaistus = new EmolevyValaistus(
        		2, "EmolevyValaistus");
        Tietokone emolevyVesiJäähdytys = new EmolevyVesiJäähdytys(
        		3, "EmolevyVesiJäähdytys");
        Tietokone emolevy = new Emolevy(
        		4, "Emolevy");
        
        
        
        EmolevyKomponentit emolevyKomponentit = new EmolevyKomponentit (
          3, "Emolevy");
        KoteloKomponentit koteloKomponentit = new KoteloKomponentit(
          3, "Kotelo");
 
        
        System.out.println("\n ----Kotelo---- \n");
        koteloKomponentit.lisääKotelonOsa(koteloTarrat);
        koteloKomponentit.lisääKotelonOsa(koteloTuulettimet);
        koteloKomponentit.lisääKotelonOsa(koteloValaistus);
        koteloKomponentit.lisääKotelonOsa(kotelo);
 
        koteloKomponentit.printTietokoneOsanHinta();
        
        System.out.println("\n ----Emolevy---- \n");
        emolevyKomponentit.lisääEmolevynOsa(emolevyExtraTuulettimet);
        emolevyKomponentit.lisääEmolevynOsa(emolevyValaistus);
        emolevyKomponentit.lisääEmolevynOsa(emolevyVesiJäähdytys);
        emolevyKomponentit.lisääEmolevynOsa(emolevy);
        
        emolevyKomponentit.printTietokoneOsanHinta();
        
        System.out.println("\n ----Muut Komponentit---- \n");
        muistipiiri.printTietokoneOsanHinta();
        näytönojain.printTietokoneOsanHinta();
        prosessori.printTietokoneOsanHinta();
        verkkokortti.printTietokoneOsanHinta();
        
        System.out.println("\n ----Yhteishinta---- \n");
        int prosessorinhinta = prosessori.printTietokoneOsanHintaRaaka();
        int verkkokortinhinta = verkkokortti.printTietokoneOsanHintaRaaka();
        int näytönohjaimenhinta = näytönojain.printTietokoneOsanHintaRaaka();
        int kotelonhinta = koteloKomponentit.printTietokoneOsanHintaRaaka();
        int emolevynhinta = emolevyKomponentit.printTietokoneOsanHintaRaaka();
        
        int summa = prosessorinhinta + verkkokortinhinta + näytönohjaimenhinta + kotelonhinta + emolevynhinta;
        
        System.out.println("Haluamasi tietokonen yhteishinta on " + summa + "e");
    }
}
