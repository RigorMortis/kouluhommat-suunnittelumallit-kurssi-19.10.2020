package compositePatternEsimerkki2;

public class SalesDepartment implements Department {
	 
    private Integer id;
    private String name;
 
    public SalesDepartment(int id, String name) {
    	this.id = id;
    	this.name = name;
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void printDepartmentName() {
        System.out.println(getClass().getSimpleName());
    }
 
    // standard constructor, getters, setters
}
