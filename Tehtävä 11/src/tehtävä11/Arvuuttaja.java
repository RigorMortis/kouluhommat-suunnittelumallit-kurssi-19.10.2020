package teht�v�11;

public class Arvuuttaja {
	private String asiakas;
	private int luku;

	public void liityPeliin(String asiakas, int luku) {
		this.asiakas = asiakas;
		this.luku = luku;
		// luku = (int)(Math.random()*6+1);
	}

	public String getAsiakas() {
		return asiakas;
	}

	public int getLuku() {
		return luku;
	}
	
	public Memento saveLukuToMemento() {
		return new Memento(luku);
	}

	public Memento saveAsiakasToMemento() {
		return new Memento(asiakas);
	}

	public void getAsiakasFromMemento(Memento memento) {
		asiakas = memento.getState();
	}
}
