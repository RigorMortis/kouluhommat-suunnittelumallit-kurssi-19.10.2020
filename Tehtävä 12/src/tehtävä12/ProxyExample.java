package teht�v�12;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class ProxyExample {
	/**
	 * Test method
	 */

	public static void main(final String[] arguments) {
		Scanner joku = new Scanner(System.in);
		Image image1 = new ProxyImage("joku1");
		Image image2 = new ProxyImage("joku2");
		ArrayList<Image> imageAlbum = new ArrayList<>();
		imageAlbum.add(image1);
		imageAlbum.add(image2);

		for (Image i : imageAlbum) {
			i.showData();
		}

		for (Image i : imageAlbum) {
			i.displayImage();
		}
		while (true) {
			System.out.println("Sy�t� picture indeksi");
			int index = joku.nextInt();
			if (imageAlbum.get(index) != null) {
				imageAlbum.get(index).displayImage();
			} else {
				System.out.println("Haetulla indeksill� ei l�ytynyt picturea");
			}
		}
	}
}