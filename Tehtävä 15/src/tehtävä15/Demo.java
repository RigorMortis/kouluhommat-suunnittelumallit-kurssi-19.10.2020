package teht�v�15;

public class Demo {

	public static void main(String args[]) {
		Dog labrador = new Labrador();
		ToyDog toyDog = new WoodenLabradorToy();

		
		ToyDog dogAdapter = new DogAdapter(labrador);

		System.out.println("\nLabrador-------------\n");
		labrador.walk();
		labrador.bark();

		System.out.println("\nWoodenLabradorToy------------\n");
		toyDog.trytobark();
		toyDog.trytowalk();

		// toy dog behaving like a real dog
		System.out.println("\nDogAdapter-----------\n");
		dogAdapter.trytowalk();
		dogAdapter.trytobark();
		
	}

}
