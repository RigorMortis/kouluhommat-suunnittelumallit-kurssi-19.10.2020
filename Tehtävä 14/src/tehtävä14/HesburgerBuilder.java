package tehtävä14;

public class HesburgerBuilder {

	public Hesburger prepareHesburger () {
		Hesburger hesburger = new Hesburger();
		hesburger.addItem(new VegePihvi());
		hesburger.addItem(new JaavuoriSalaatti());
		hesburger.addItem(new SinaappiKastike());
		hesburger.addItem(new PaarynaHedelmä());
		return hesburger;
	}
}
