package teht�v�6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;

public class Main_Pizzeria {
	static int count = 1;
	static int choice;

	public static void main(String[] args) throws NumberFormatException, IOException {

		Pizza kolmioPizza = new KolmioPizza();

		Pizza suorakulmioPizza = new SuorakulmioPizza();

		Pizza ananas = new AnanasPizzaDecorator(new KolmioPizza());

		Pizza liha = new LihaPizzaDecorator(new SuorakulmioPizza());

		Pizza herkkusieni = new HerkkuSieniPizzaDecorator(new KolmioPizza());

		Pizza juusto = new JuustoPizzaDecorator(new KolmioPizza());

		Pizza sinihomejuusto = new SinihomejuustoPizzaDecorator(new KolmioPizza());

		Pizza tomaatti = new TomaattiKastikePizzaDecorator(new KolmioPizza());

		Pizza tonnikala = new TonnikalaPizzaDecorator(new KolmioPizza());

		ArrayList<String> valmispizza = new ArrayList<String>();
		
		int hinta = 0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		do {
			System.out.println("T�yte Valikko, pizza pohja kuuluu hintaan!");
			System.out.println(" --------------------- ");
			System.out.println(" 1. Ananas -- 2e ");
			System.out.println(" 2. HerkkuSieni -- 3e     ");
			System.out.println(" 3. Juusto -- 4e  ");
			System.out.println(" 4. Liha -- 6e    ");
			System.out.println(" 5. Sinihomejuusto -- 4e   ");
			System.out.println(" 6. Tomaatti -- 2e   ");
			System.out.println(" 7. Tonnikala -- 1e   ");
			System.out.println(" 8. Tarkista t�ytteet    ");
			System.out.println(" 9. Poistu      ");

			System.out.print("\n");
			System.out.print("Mit� haluat tehd�? ");

			choice = Integer.parseInt(br.readLine());
			switch (choice) {

			case 1: {
				System.out.println("\n");
				ananas.print();
				valmispizza.add("ananas");
				hinta += ananas.hinta();
				System.out.println("\n");

				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // End of case 1
				break;
			case 2: {
				System.out.println("\n");
				herkkusieni.print();
				valmispizza.add("herkkusieni");
				hinta += herkkusieni.hinta();
				System.out.println("\n");
				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // End of case 2
				break;
			case 3: {
				System.out.println("\n");
				juusto.print();
				valmispizza.add("juusto");
				hinta += juusto.hinta();
				System.out.println("\n");

				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // End of case 3
				break;
			case 4: {
				System.out.println("\n");
				liha.print();
				valmispizza.add("liha");
				hinta += liha.hinta();
				System.out.println("\n");
				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // end of case 4
				break;
			case 5: {
				System.out.println("\n");
				sinihomejuusto.print();
				valmispizza.add("sinihomejuusto");
				hinta += sinihomejuusto.hinta();
				System.out.println("\n");
				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // end of case 4
				break;
			case 6: {
				System.out.println("\n");
				tomaatti.print();
				valmispizza.add("tomaatti");
				hinta += tomaatti.hinta();
				System.out.println("\n");
				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // end of case 4
				break;
			case 7: {
				System.out.println("\n");
				tonnikala.print();
				valmispizza.add("tonnikala");
				hinta += tonnikala.hinta();
				System.out.println("\n");
				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // end of case 4
				break;
			case 8: {
				System.out.println("\n");
				System.out.println("Olet valinnut :");
				System.out.println(valmispizza);
				System.out.println("Kokonaishinta on " + hinta + "�");
				System.out.println("\n");
				// System.out.println("Press Enter key to continue...");
				// System.in.read();

			} // end of case 4
				break;

			default:
				return;
			}

		} while (choice != 9);

	}
}
