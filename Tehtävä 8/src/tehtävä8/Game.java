package teht�v�8;

public abstract class Game {

	abstract void initialize();

	abstract void startPlay();

	abstract void endPlay();
	
	protected int playersCount;
	
	abstract void printWinner();
	
	abstract boolean playAgain();
	
	abstract void makePlay(int player);
	
	public final void play() {
		while (playAgain() == true) {
		initialize();
		startPlay();
		endPlay();
		}
	}

}
