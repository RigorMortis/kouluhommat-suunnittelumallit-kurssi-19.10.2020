package esimerkki1;

public interface ComputerPart {
	   public void accept(ComputerPartVisitor computerPartVisitor);
	}
