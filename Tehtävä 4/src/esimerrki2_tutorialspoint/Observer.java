package esimerrki2_tutorialspoint;

public abstract class Observer {
	   protected Subject subject;
	   public abstract void update();
	}

