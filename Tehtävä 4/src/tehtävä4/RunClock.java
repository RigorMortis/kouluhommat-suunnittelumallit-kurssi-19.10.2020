package teht�v�4;

import java.util.Observable;
import java.util.Observer;



public class RunClock implements Observer {

	private DigitalClock clock = new DigitalClock();
	
	public RunClock() {
		clock.addObserver(this);
	}
	
	public static void main(String[] args){
		javax.swing.SwingUtilities.invokeLater(new Runnable() {  
			@Override
			public void run() {  
				RunClock main = new RunClock();
			}
		});   
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Time is: " + arg1);
			}
}

