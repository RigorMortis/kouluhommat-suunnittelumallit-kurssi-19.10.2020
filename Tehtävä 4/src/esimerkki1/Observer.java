package esimerkki1;



public interface Observer {
	public abstract void update(
			Subject theChangedSubject);
}
