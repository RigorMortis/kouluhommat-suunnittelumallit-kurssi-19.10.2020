package teht�v�5;

public class DebugLogHelper {

	private static DebugLogHelper log = null;
	 
    private DebugLogHelper() {
        super();
    }
 
    public static DebugLogHelper getInstance() {
        if (log == null) {
            synchronized (DebugLogHelper.class) {
                if (log == null) {
                    log = new DebugLogHelper();
                    //system.out.print jotain
                }
            }
        }
 
        return log;
    }
 
}
