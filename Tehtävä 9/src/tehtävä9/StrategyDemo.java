package teht�v�9;



public class StrategyDemo {
	 public static void main(String[] args) {
	      Context context = new Context(new SelectSort());	
	      System.out.println("SelectSort!");
	      System.out.println("----------------------------------------------------------------");
	      context.executeStrategy();
	      

	      context = new Context(new MergeSort());
	      System.out.println("MergeSort!");
	      System.out.println("----------------------------------------------------------------");
	      context.executeStrategy();
	     

	      context = new Context(new QuickSort());
	      System.out.println("QuickSort!");
	      System.out.println("----------------------------------------------------------------");
	      context.executeStrategy();
	      
	   }
}
