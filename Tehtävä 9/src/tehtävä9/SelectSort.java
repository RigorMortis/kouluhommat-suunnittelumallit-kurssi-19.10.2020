package teht�v�9;

import java.util.Random;



public class SelectSort implements Strategy {
	 final static int MAX=25000;
	 static long laskuri = 0;
	 
	@Override
	public void doSorting() {
		// TODO Auto-generated method stub
		selectSort();
        laskuri = 0;
	}
	 public static void selectSort() {

	        int[] taul = new int[MAX]; //taul tallettaa lajiteltavat tiedot
	        int i, j, k, apu, pienin;
	        Random r = new Random(); //luodaan satunnaislukugeneraattori
	        System.out.println("Generoidaan syottoaineisto: ");
	        for (i=0;i<MAX;i++) {

	            taul[i] = r.nextInt(1000); //generoidaan luvut
	            System.out.print(taul[i]+" ");
	            if (i>0 && i%40==0) // rivinvaihto
	                System.out.println();
	        }
	        System.out.println("\nSuoritetaan valintalajittelu, paina Enter ");
	        Lue.merkki();
	        
	        laskuri++; //ulompi for lause
	        for (i=0;i<MAX;i++) {
	            pienin=i;
	            laskuri++; // sisempi for lause alla
	            for (j=i+1;j<MAX;j++) {
	                /* loytyyko taulukon loppupaasta pienempaa alkiota? */
	            	laskuri++; //if lause alla
	                if (taul[j] < taul[pienin]) {
	                    pienin=j;
	                }
	                laskuri++; //sisempi for lause
	            }
	            laskuri++; // if lause alla
	            if (pienin != i) {
	                /* jos loytyi suoritetaan alkioiden vaihto */
	                apu=taul[pienin];
	                taul[pienin]=taul[i];
	                taul[i]=apu;
	            }
	            laskuri++; // ulompi for lause
	        }
	        System.out.println();
	        for (i=0;i<MAX;i++) {
	            System.out.print(taul[i]+" ");
	            if (i>0 && i%40==0) // rivinvaihto
	                System.out.println();
	        }
	        System.out.println("\nKuittaa tulos, paina Enter laskuri = " + laskuri );
	        Lue.merkki();

	    }

}
