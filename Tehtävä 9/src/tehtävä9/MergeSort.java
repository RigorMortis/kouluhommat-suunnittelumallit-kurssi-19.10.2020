package teht�v�9;

import java.util.Random;


public class MergeSort implements Strategy {
	 static long laskuri = 0;
	 final static int MAX=25000;
	 

	 @Override
		public void doSorting() {
			// TODO Auto-generated method stub
			mergeSort();
			laskuri = 0;
		}
	
    public static void mergeSort() {
        int[] a= new int[MAX];
        int i;
        Random r = new Random(); //luodaan satunnaislukugeneraattori
        System.out.println("\nGeneroidaan syottoaineisto: ");
        
        for (i=0;i<MAX;i++) {
        	
            a[i] = r.nextInt(1000); //generoidaan luvut
            
            System.out.print(a[i]+" ");
            
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
            
        }
       
        System.out.println("\nSuoritetaan lomituslajittelu, paina Enter ");
        Lue.merkki();

        mergeSort(a, 0, MAX-1);
        for (i=0;i<MAX;i++) {
            System.out.print(a[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        System.out.println("\nKuittaa tulos, paina Enter laskuri = " + laskuri);
        Lue.merkki();
    }

    private static int[] tau = new int[MAX]; // aputaulukko (ei varata tata pinosta!)

    //oletus: osataulukot t[p..q] ja t[q+1...r] ovat jarjestyksess„
    public static void merge(int t[], int p, int q, int r) {
        //i osoittaa 1. osataulukkoa, j osoittaa 2. osataulukkoa
        // k osoittaa aputaulukkoa, johon yhdiste kirjoitetaan.
        int i=p, j=q+1, k=0;
        laskuri+=2;
        while(i<q+1 && j<r+1) {
        	laskuri++;
                if (t[i]<t[j]) {
                        tau[k++]=t[i++];
                }
                else {
                        tau[k++]=t[j++];
                }
                laskuri+=2;
        }
        //toinen osataulukko kasitelty, siirretaan toisen kasittelemattomat
        laskuri++;
        while (i<q+1) {
                tau[k++]=t[i++];
                laskuri++;
        }
        laskuri++;
        while (j<r+1) {
                tau[k++]=t[j++];
        //siirretaan yhdiste alkuperaiseen taulukkoon
                laskuri++;
        }
        laskuri++;
        for (i=0;i<k;i++) {
        	laskuri++;
                t[p+i]=tau[i];
        }
    }

    public static void mergeSort(int t[],  int alku,  int loppu) {
        int ositus;
        long la, ll, lt;
        laskuri++;
        if (alku<loppu) { //onko vah. 2 alkiota, etta voidaan suorittaa ositus

                la=alku; ll=loppu;
                lt=(la+ll)/2;
                ositus=(int)lt;
                mergeSort(t, alku, ositus);//lajitellaan taulukon alkupaa
                mergeSort(t, ositus+1, loppu);//lajitellaan taulukon loppupaa
                merge(t, alku, ositus, loppu);//yhdistetaan lajitellut osataulukot
        }

    }

	

	

	

}
